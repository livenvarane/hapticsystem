#import <AudioToolbox/AudioServices.h>

@interface SBAlertItem : NSObject
@end

@interface HapticSystemNoAlertItem : SBAlertItem
+(void)activateAlertItem:(id)arg1;
-(id)alertController;
-(void)dismiss;
@end

@interface CHHapticEngine : NSObject
+(id)capabilitiesForHardware;
@end

@interface CCUIButtonModuleViewController : UIViewController
@property (assign,getter=isExpanded,nonatomic) BOOL expanded;
@end

@interface SBNestingViewController : UIViewController
@end

@interface SBFolderController : SBNestingViewController
@end

@interface SBRootFolderController : SBFolderController
@property (getter=isOnTodayPage,nonatomic,readonly) BOOL todayPage; 
@end

@protocol CHHapticDeviceCapability
@property (readonly) BOOL supportsHaptics; 

-(BOOL)supportsHaptics;
@end