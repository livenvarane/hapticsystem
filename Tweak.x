#import "HapticSystem.h"

%group LoadHS

  %hook SBBiometricEventLogger

  -(void)_unlockAnimationWillStart:(id)arg1 {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id unlockHapticType = [bundleDefaults valueForKey:@"deviceUnlock"];

    if([unlockHapticType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([unlockHapticType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([unlockHapticType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![unlockHapticType isEqual:@4]) {
      AudioServicesPlaySystemSound(1520);
    }
  }

  %end

  %hook SBVolumeControl

  -(void)increaseVolume {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id volumeChangeType = [bundleDefaults valueForKey:@"volumeChange"];

    if([volumeChangeType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([volumeChangeType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([volumeChangeType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![volumeChangeType isEqual:@4]) {
      AudioServicesPlaySystemSound(1519);
    }
  }

  -(void)decreaseVolume {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id volumeChangeType = [bundleDefaults valueForKey:@"volumeChange"];

    if([volumeChangeType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([volumeChangeType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([volumeChangeType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![volumeChangeType isEqual:@4]) {
      AudioServicesPlaySystemSound(1519);
    }
  }

  %end

  %hook SBIconController

  -(void)iconManager:(id)arg1 launchIconForIconView:(id)arg2 {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id appLaunchType = [bundleDefaults valueForKey:@"appLaunch"];

    if([appLaunchType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([appLaunchType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([appLaunchType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
  }

  %end

  %hook SBCombinationHardwareButton

  -(void)screenshotGesture:(id)arg1 {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id screenshotType = [bundleDefaults valueForKey:@"screenshot"];

    if([screenshotType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([screenshotType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([screenshotType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![screenshotType isEqual:@4]) {
      AudioServicesPlaySystemSound(1521);
    }
  }

  %end

  %hook SBSleepWakeHardwareButtonInteraction

  -(void)_performSleep {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id lockType = [bundleDefaults valueForKey:@"deviceLock"];

    if([lockType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([lockType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([lockType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![lockType isEqual:@4]) {
      AudioServicesPlaySystemSound(1520);
    }
  }

  -(void)_performWake {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id wakeType = [bundleDefaults valueForKey:@"deviceWake"];

    if([wakeType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([wakeType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([wakeType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
  }

  %end

  %hook CCUIButtonModuleViewController // Big buttons

  // -(void)buttonTouchDown:(id)arg1 forEvent:(id)arg2 {  //Even if button tap is cancelled
  //   %orig;

  //   AudioServicesPlaySystemSound(1521);
  // }

  // -(void)_buttonTouchDown:(id)arg1 forEvent:(id)arg2 {
  //   %orig;

  //   AudioServicesPlaySystemSound(1521);
  // }

  -(void)_buttonTapped:(id)arg1 forEvent:(id)arg2 {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id CCButtonType = [bundleDefaults valueForKey:@"CCButtonTapMode"];

    if([CCButtonType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([CCButtonType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([CCButtonType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![CCButtonType isEqual:@4]) {
      AudioServicesPlaySystemSound(1520);
    }
  }

  -(void)willTransitionToExpandedContentMode:(BOOL)arg1 {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id leaveExtendedType = [bundleDefaults valueForKey:@"leavingExtendedMode"];
    id enterExtendedType = [bundleDefaults valueForKey:@"enteringExtendedMode"];

    if(self.isExpanded == FALSE) { //Leaving extended mode
      if([leaveExtendedType isEqual:@1]) {
        AudioServicesPlaySystemSound(1519);
      }
      else if([leaveExtendedType isEqual:@2]) {
        AudioServicesPlaySystemSound(1520);
      }
      else if([leaveExtendedType isEqual:@3]) {
        AudioServicesPlaySystemSound(1521);
      }
      else if(![leaveExtendedType isEqual:@4]) {
        AudioServicesPlaySystemSound(1519);
      }
    }
    else { //Entering
      if([enterExtendedType isEqual:@1]) {
        AudioServicesPlaySystemSound(1519);
      }
      else if([enterExtendedType isEqual:@2]) {
        AudioServicesPlaySystemSound(1520);
      }
      else if([enterExtendedType isEqual:@3]) {
        AudioServicesPlaySystemSound(1521);
      }
      else if(![enterExtendedType isEqual:@4]) {
        AudioServicesPlaySystemSound(1520);
      }
    }
  }

  %end

  %hook CCUILabeledRoundButton

  -(void)buttonTapped:(id)arg1 { //Connectivity module buttons
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id CCButtonType = [bundleDefaults valueForKey:@"CCButtonTapMode"];

    if([CCButtonType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([CCButtonType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([CCButtonType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
    else if(![CCButtonType isEqual:@4]) {
      AudioServicesPlaySystemSound(1520);
    }
  }

  %end

  %hook SBControlCenterController

  -(void)_willBeginTransition {
    %orig;

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id CCOpeningType = [bundleDefaults valueForKey:@"CCOpening"];

    if([CCOpeningType isEqual:@1]) {
      AudioServicesPlaySystemSound(1519);
    }
    else if([CCOpeningType isEqual:@2]) {
      AudioServicesPlaySystemSound(1520);
    }
    else if([CCOpeningType isEqual:@3]) {
      AudioServicesPlaySystemSound(1521);
    }
  }

  %end

  %hook SBRootFolderController

  -(void)willBeginTransitionToState:(long long)arg1 animated:(BOOL)arg2 interactive:(BOOL)arg3 {
    %orig;

    //0.0 = leave wid view; 1.0 = opening spot; 2.0 = opening wid AND closing wid spotlight view
    //3.0 = opening wid spotlight view

    NSDictionary *bundleDefaults = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.hapticprefs"];

    id TodayLeaveType = [bundleDefaults valueForKey:@"HStodayClose"];
    id TodayOpeningType = [bundleDefaults valueForKey:@"HStodayOpen"];
    id SpotlightType = [bundleDefaults valueForKey:@"HSspotlight"];

    if(arg1 == 0.0) { // Leaving Today Page
      if([TodayLeaveType isEqual:@1]) {
        AudioServicesPlaySystemSound(1519);
      }
      else if([TodayLeaveType isEqual:@2]) {
        AudioServicesPlaySystemSound(1520);
      }
      else if([TodayLeaveType isEqual:@3]) {
        AudioServicesPlaySystemSound(1521);
      }
      else if(![TodayLeaveType isEqual:@4]) {
        AudioServicesPlaySystemSound(1519);
      }
    }
    else if(arg1 == 1.0) { //Opening Spotilight View
      if([SpotlightType isEqual:@1]) {
        AudioServicesPlaySystemSound(1519);
      }
      else if([SpotlightType isEqual:@2]) {
        AudioServicesPlaySystemSound(1520);
      }
      else if([SpotlightType isEqual:@3]) {
        AudioServicesPlaySystemSound(1521);
      }
      else if(![SpotlightType isEqual:@4]) {
        AudioServicesPlaySystemSound(1519);
      }
    }
    else if(arg1 == 2.0) { //Opening Today Page
      if(self.isOnTodayPage == FALSE) { //If true user is closing Today Page Spotlight view
        if([TodayOpeningType isEqual:@1]) {
          AudioServicesPlaySystemSound(1519);
        }
        else if([TodayOpeningType isEqual:@2]) {
          AudioServicesPlaySystemSound(1520);
        }
        else if([TodayOpeningType isEqual:@3]) {
          AudioServicesPlaySystemSound(1521);
        }
        else if(![TodayOpeningType isEqual:@4]) {
          AudioServicesPlaySystemSound(1519);
        }
      }
      else {
        if([SpotlightType isEqual:@1]) {
          AudioServicesPlaySystemSound(1519);
        }
        else if([SpotlightType isEqual:@2]) {
          AudioServicesPlaySystemSound(1520);
        }
        else if([SpotlightType isEqual:@3]) {
          AudioServicesPlaySystemSound(1521);
        }
        else if(![SpotlightType isEqual:@4]) {
          AudioServicesPlaySystemSound(1519);
        }
      }
    }
    else if(arg1 == 3.0) { //Opening Today Page Spotlight View 
      if([SpotlightType isEqual:@1]) {
        AudioServicesPlaySystemSound(1519);
      }
      else if([SpotlightType isEqual:@2]) {
        AudioServicesPlaySystemSound(1520);
      }
      else if([SpotlightType isEqual:@3]) {
        AudioServicesPlaySystemSound(1521);
      }
      else if(![SpotlightType isEqual:@4]) {
        AudioServicesPlaySystemSound(1519);
      }
    }
  }

  %end

%end

%group unsuported

  // https://www.reddit.com/r/jailbreakdevelopers/comments/b3gbec/help_showing_uialert_after_respring/ej6koko/
  
  %subclass HapticSystemNoAlertItem : SBAlertItem
    -(void)configure:(BOOL)arg1 requirePasscodeForActions:(BOOL)arg2 {
      UIAlertController *alertController = [self alertController];
      [alertController setTitle:@"Your iOS version is not supported!"];
      [alertController setMessage:@"HapticSystem is not supposed to work with iOS 14 and more.\nPlease uninstall it :("];

      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [self dismiss];
      }];

      [alertController addAction:ok];
    }

    -(BOOL)reappearsAfterUnlock {
      return YES;
    }

    -(void)dismiss {
      %orig;
    }
  %end

  %hook SBHomeScreenViewController

  - (void)viewDidLoad {
      %orig;

      SBAlertItem *item = [[%c(HapticSystemNoAlertItem) alloc] init];
      [[item class] activateAlertItem:item];
  }
  %end

%end

%group oldiOS

  // https://www.reddit.com/r/jailbreakdevelopers/comments/b3gbec/help_showing_uialert_after_respring/ej6koko/
  
  %subclass HapticSystemNoAlertItem : SBAlertItem
    -(void)configure:(BOOL)arg1 requirePasscodeForActions:(BOOL)arg2 {
      UIAlertController *alertController = [self alertController];
      [alertController setTitle:@"Your iOS version is not supported!"];
      [alertController setMessage:@"HapticSystem does not support your iOS version (Only iOS 13.x).\nPlease uninstall it :("];

      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [self dismiss];
      }];

      [alertController addAction:ok];
    }

    -(BOOL)reappearsAfterUnlock {
      return YES;
    }

    -(void)dismiss {
      %orig;
    }
  %end

  %hook SBHomeScreenViewController

  - (void)viewDidLoad {
      %orig;

      SBAlertItem *item = [[%c(HapticSystemNoAlertItem) alloc] init];
      [[item class] activateAlertItem:item];
  }
  %end

%end

%group NoHS

  // https://www.reddit.com/r/jailbreakdevelopers/comments/b3gbec/help_showing_uialert_after_respring/ej6koko/
  
  %subclass HapticSystemNoAlertItem : SBAlertItem
    -(void)configure:(BOOL)arg1 requirePasscodeForActions:(BOOL)arg2 {
      UIAlertController *alertController = [self alertController];
      [alertController setTitle:@"Your device is not supported!"];
      [alertController setMessage:@"HapticSystem will not work because your device does not support haptic feedbacks.\nPlease uninstall it :("];

      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [self dismiss];
      }];

      [alertController addAction:ok];
    }

    -(BOOL)reappearsAfterUnlock {
      return YES;
    }

    -(void)dismiss {
      %orig;
    }
  %end

  %hook SBHomeScreenViewController

  - (void)viewDidLoad {
      %orig;

      SBAlertItem *item = [[%c(HapticSystemNoAlertItem) alloc] init];
      [[item class] activateAlertItem:item];
  }
  %end

%end

%ctor {
  BOOL supportHapticsValue = [[%c(CHHapticEngine) capabilitiesForHardware] supportsHaptics];
  NSString *deviceVersion = [[UIDevice currentDevice] systemVersion];
  float floatVersion = [deviceVersion floatValue];

  if(floatVersion >= 13.0) {
    if(supportHapticsValue == TRUE) {
    %init(LoadHS)
  }
  else {
    %init(NoHS)
  }
  }
  else if(floatVersion >= 14.0) {
    %init(unsuported)
  }
  else {
    %init(oldiOS)
  }
}