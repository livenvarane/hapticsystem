#include "HSPRootListController.h"

@implementation HSPRootListController

- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"Root" target:self];
	}

	return _specifiers;
}

- (void)openReddit {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"https://www.reddit.com/user/LVN_N"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Reddit url opened");
        }
    }];
}

- (void)openTwitter {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"https://www.twitter.com/LivenOff"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Twitter url opened");
        }
    }];
}

@end

@interface HomeScreenPrefsController : PSListController
@end

@implementation HomeScreenPrefsController
- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"HomeScreen" target:self];
	}
	[(UINavigationItem *)self.navigationItem setTitle:@"HomeScreen"];
	return _specifiers;
}
@end

@interface LockScreenPrefsController : PSListController
@end

@implementation LockScreenPrefsController
- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"LockScreen" target:self];
	}
	[(UINavigationItem *)self.navigationItem setTitle:@"LockScreen"];
	return _specifiers;
}
@end

@interface ButtonRelatedPrefsController : PSListController
@end

@implementation ButtonRelatedPrefsController
- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"ButtonRelated" target:self];
	}
	[(UINavigationItem *)self.navigationItem setTitle:@"Button Related"];
	return _specifiers;
}
@end

@interface ControlCenterPrefsController : PSListController
@end

@implementation ControlCenterPrefsController
- (NSArray *)specifiers {
	if (!_specifiers) {
		_specifiers = [self loadSpecifiersFromPlistName:@"ControlCenter" target:self];
	}
	[(UINavigationItem *)self.navigationItem setTitle:@"Control Center"];
	return _specifiers;
}
@end