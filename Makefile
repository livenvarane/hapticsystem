ARCHS = arm64 arm64e
export SDKVERSION=13.0
TARGET=iphone:clang::13.0

INSTALL_TARGET_PROCESSES = SpringBoard

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = HapticSystem

HapticSystem_FILES = Tweak.x
HapticSystem_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk
SUBPROJECTS += hapticprefs
include $(THEOS_MAKE_PATH)/aggregate.mk
